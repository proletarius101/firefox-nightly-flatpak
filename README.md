## How to install

<a href='https://gitlab.com/proletarius101/firefox-nightly-flatpak/-/raw/main/firefox-nightly.flatpakref?inline=false'><img width='240' alt='Download on Flathub' src='assets/download-badge.svg'/></a>

Or at the command line,

````bash
flatpak install --user https://gitlab.com/proletarius101/firefox-nightly-flatpak/raw/master/firefox-nightly.flatpakref
````
